package ast;

import interp.Closure;
import interp.Env;
import interp.Value;

public class Function extends Term {
    private final Var argument;
    private final Term execution;
    public Function(Var argument, Term execution) {
        this.argument = argument;
        this.execution = execution;
    }

    public Var getArgValue() {
        return argument;
    }

    public Term getExecution() {
        return execution;
    }
    @Override
    public Value interp(Env e) {
        return new Closure(argument, execution,e);
    }
}
