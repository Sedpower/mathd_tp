package ast;

import ast.Function;
import ast.Term;
import interp.*;

public class TermTerm extends Term {
    Term execution;
    Term argument;
    public TermTerm(Term execution, Term argument) {
        this.execution = execution;
        this.argument = argument;
    }

    @Override
    public Value interp(Env e) {
        Closure closure = (Closure) execution.interp(e);
        closure.setBlockEnv(closure.getBlockEnv().add(closure.getArgument().varName, this.argument.interp(e)));
        if (closure.getFunction() instanceof Function function) {
            return new Closure(function.getArgValue(), function.getExecution(), closure.getBlockEnv());
        }
        return closure.getFunction().interp(closure.getBlockEnv());
    }
}
