grammar PCF;

// règles syntaxiques

program : term EOF ;
term : LIT                                   # Lit
     | '(' term ')'                          # Par
     | term term                             # TermTerm
     | term OPP term                         # BinOp
     | term OP term                          # BinOp
     | 'ifz' term 'then' term 'else' term    # Cond
     |  VAR                                  # Var
     | 'let' VAR '=' term 'in' term          # LetIn // let x = 1 in 1 + x
     | 'fun' VAR '->' term                   # Function // fun x -> x + 1
     ;

// règles lexicales

VAR : [a-zA-Z]+ ;
ASSIGN : '=';
OPP: '*' | '/';
OP : '+' | '-' ;
LIT : '0' | [1-9][0-9]* ;
WS : [ \t\n\r]+ -> channel(HIDDEN) ;
LINE_COMMENT : '//' ~'\n'* '\n' -> channel(HIDDEN) ;