package interp;

public class IntVal extends Value {
    public int value;

    public IntVal(int i) {
        this.value = i;
    }

    @Override
    public String toString() {
        return Integer.toString(value);
    }
}
