package interp;

import java.util.Optional;

public class EmptyEnv extends Env {
    @Override
    public boolean isEmpty() {
        return true;
    }

    @Override
    public Binding last() {
        return null;
    }

    @Override
    public Env previous() {
        return null;
    }

    @Override
    public Optional<Value> lookup(String id) {
        return Optional.empty();
    }

    @Override
    public String toString() {
        return "EmptyEnv{}";
    }
}
