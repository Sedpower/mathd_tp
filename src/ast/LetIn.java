package ast;

import interp.Env;
import interp.Value;

public class LetIn extends Term {
    String name;
    Term value;
    Term link;

    public String getName() {
        return name;
    }

    public Term getValue() {
        return value;
    }

    public LetIn(String name, Term value, Term link) {
        this.name = name;
        this.value = value;
        this.link = link;
    }

    @Override
    public Value interp(Env e) {
        e = e.add(name,value.interp(e));
        return link.interp(e);
    }
}