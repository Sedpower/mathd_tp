package interp;

public class VariableNotDeclaredException extends RuntimeException {
    public VariableNotDeclaredException(String variableName){
        super("Variable " + variableName + " is not declared");
    }
}
