package interp;

import java.util.Optional;

public class NonEmptyEnv extends Env {
    private final Binding last;
    private final Env previous;

    public NonEmptyEnv(Binding last, Env previous) {
        this.last = last;
        this.previous = previous;
    }


    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public Binding last() {
        return last;
    }

    @Override
    public Env previous() {
        return previous;
    }

    @Override
    public Optional<Value> lookup(String id) {
        return this.last.getName().equals(id) ? Optional.of(this.last.getValue()) : this.previous().lookup(id);
    }
}
