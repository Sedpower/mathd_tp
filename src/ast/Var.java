package ast;

import interp.Env;
import interp.Value;
import interp.VariableNotDeclaredException;

public class Var extends Term {
    public String varName;

    public Var(String varName) {
        this.varName = varName;
    }

    @Override
    public Value interp(Env e) {
        return e.lookup(varName).orElseThrow(() -> new VariableNotDeclaredException(varName));
    }
}