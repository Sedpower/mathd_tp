package interp;

import ast.LetIn;
import ast.Term;
import ast.Var;

public class Closure extends Value {
    Term function;
    Var argument;
    Env blockEnv;

    public Closure(Var argument, Term function, Env blockEnv) {
        this.argument = argument;
        this.function = function;
        this.blockEnv = blockEnv;
    }

    public Term getFunction() {
        return function;
    }
    public Var getArgument(){
        return argument;
    }

    @Override
    public String toString() {
        return "Closure{" +
                "\n    function=" + function +
                ",\n    argument=" + argument +
                ",\n    blockEnv=" + blockEnv +
                "\n}";
    }

    public Env getBlockEnv() {
        return blockEnv;
    }

    public void setBlockEnv(Env blockEnv) {
        this.blockEnv = blockEnv;
    }
}
