package parser;

import ast.*;
import org.antlr.v4.runtime.tree.TerminalNode;

import java.util.ArrayList;
import java.util.List;

public class ASTVisitor extends PCFBaseVisitor<AST> {

    @Override
    public AST visitLit(PCFParser.LitContext ctx) {
        return new Lit(Integer.parseInt(ctx.getText()));
    }

    @Override
    public AST visitPar(PCFParser.ParContext ctx){
        return visit(ctx.term());
    }

    @Override
    public AST visitBinOp(PCFParser.BinOpContext ctx) {
        TerminalNode feuille;
        feuille = ctx.OPP();
        if (feuille == null)
            feuille = ctx.OP();
        OP op = OP.parseOP(feuille.getText());
        List<PCFParser.TermContext> ANTLRTerms = ctx.term();
        List<Term> terms = new ArrayList<>();
        for (PCFParser.TermContext ANTLRTerm : ANTLRTerms)
            terms.add((Term) visit(ANTLRTerm));
        return new BinOp(op, terms.get(0), terms.get(1));
    }

    @Override
    public AST visitFunction(PCFParser.FunctionContext ctx) {
        return new Function(new Var(ctx.VAR().getText()), (Term) visit(ctx.term()));
    }

    @Override
    public AST visitLetIn(PCFParser.LetInContext ctx) {
        TerminalNode var = ctx.VAR();
        var.getText();
        return new LetIn(ctx.VAR().getText(),(Term) visit(ctx.term(0)), (Term) visit(ctx.term(1)));
    }

    @Override
    public AST visitVar(PCFParser.VarContext ctx) {
        return new Var(ctx.VAR().getText());
    }

    @Override
    public AST visitCond(PCFParser.CondContext ctx) {
        List<PCFParser.TermContext> ANTLRTerms = ctx.term();
        List<Term> terms = new ArrayList<>();
        for (PCFParser.TermContext ANTLRTerm : ANTLRTerms)
            terms.add((Term) visit(ANTLRTerm));
        return new Cond(terms.get(0), terms.get(1), terms.get(2));
    }

    @Override
    public AST visitTermTerm(PCFParser.TermTermContext ctx) {
        List<PCFParser.TermContext> ANTLRTerms = ctx.term();
        List<Term> terms = new ArrayList<>();
        for (PCFParser.TermContext ANTLRTerm : ANTLRTerms)
            terms.add((Term) visit(ANTLRTerm));
        return new TermTerm(terms.get(0),terms.get(1));
    }
}